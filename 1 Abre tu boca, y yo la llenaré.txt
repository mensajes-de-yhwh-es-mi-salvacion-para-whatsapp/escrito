*Abre tu boca, y yo la llenaré*

_*Salmos 81:10* Yo soy Jehová tu Dios, Que te hice subir de la tierra de Egipto; *Abre tu boca, y yo la llenaré.*_

Palabras de Jehová «Abre tu boca», creo que en ocasiones nos preguntamos que orar o que decir en una oración, lo más bello de esto es que viene Dios y nos dice «Abre tu boca» palabra que viene juntamente con una promesa «y yo la llenaré».

Debemos confiar en Dios que él llenará nuestra boca con sus palabras para poder acercarnos a El en oración porque él suple todas nuestras necesidades, cuanto más nuestra necesidad de orar para acercarnos a Él, cuanto más si Él tiene mayor necesidad en que oremos, así mismo viene y nos alienta diciendo «Abre tu boca, y yo la llenaré».

_*Porque Él suplirá mi necesidad de orar.*_

_*Filipenses 4:19* Mi Dios, pues, suplirá todo lo que os falta conforme a sus riquezas en gloria en Cristo Jesús._

_*Jeremías 20:9 DHH* Si digo: «No pensaré más en el Señor, no volveré a hablar en su nombre», entonces tu palabra en mi interior se convierte en un fuego que devora, que me cala hasta los huesos. Trato de contenerla, pero no puedo._

_*Salmos 71:8* Sea llena mi boca de tu alabanza, De tu gloria todo el día._

[*DHH*]: Biblia Dios Habla Hoy