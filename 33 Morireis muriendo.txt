*Muriendo morirás*

_*Génesis 2:17* mas del árbol de la ciencia del bien y del mal no comerás; porque el día que de él comieres, ciertamente morirás._

En Génesis 2:17 Dios habla con Adán y le indica lo que sucederá al comer del árbol de la ciencia del bien y del mal, y claramente especifica que morirá.

Es interesante porque cuando vemos en el original aparece la palabra morir dos veces, es decir, en el original dice «morir morir» y esto es un hebraismo lo cual significa «muriendo morirás». Y luego vemos que Adán no muere del todo, porque se tiene que cumplir la palabra que «morirá muriendo». 

Cuando vemos esto con respecto a nuestra vida nos damos cuenta que Dios nos da un cuerpo el cual llega a una cúspide y luego va en decadencia, es decir, que comenzamos a morir, nuestra vida va muriendo poco a poco, se pasa a la edad adulta, luego a la vejez y luego morimos.

Y esto del envejecimiento para luego morir solo es un cumplimiento de lo que le dijeron a Adán, «muriendo morirás».

Con esto debemos comprender que Dios en su sabiduría nos está dando a entender que el juicio esta vigente a menos que creamos en Cristo Jesús que nos rescata de la vida que nos fue dada a causa del pecado de Adán y que en Cristo Jesús somo nuevas criaturas que viviremos por siempre lejos del juicio que le fue dado a Adán por desobediencia. Y que este cuerpo de muerte el cual tenemos es una manera de hacernos reflexionar que somos hombres que pecamos y necesitamos de la redención de Cristo Jesús y ser librados de este cuerpo de muerte, el cual será cambiado en la venida de Cristo Jesús.

_*Romanos 7:24-25* 24 !!Miserable de mí! ¿quién me librará de este cuerpo de muerte? 25 Gracias doy a Dios, por Jesucristo Señor nuestro. Así que, yo mismo con la mente sirvo a la ley de Dios, mas con la carne a la ley del pecado._

_*1 Corintios 15:52* en un momento, en un abrir y cerrar de ojos, a la final trompeta; porque se tocará la trompeta, y los muertos serán resucitados incorruptibles, y nosotros seremos transformados._

Así que esperanza tenemos en Cristo porque nos revela su verdad. La transición de la vida y el deterioro de este cuerpo solo nos da a conocer que él es justo y nos deja ver el juicio de Adán para nosotros poder clamar por salvación y recibir la redención de Cristo y aunque muramos saber que el que está en Cristo aunque este muerto vivirá y que el nos librará de este cuerpo de muerte. Gloria al Rey de reyes y Señor de Señores a Jesucristo, por los siglos de los siglos. Amén.