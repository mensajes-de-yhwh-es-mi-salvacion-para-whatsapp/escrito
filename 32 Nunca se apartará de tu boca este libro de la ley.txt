*Nunca se apartará de tu boca este libro de la ley*

_*Josué 1:8* Nunca se apartará de tu boca este libro de la ley, sino que de día y de noche meditarás en él, para que guardes y hagas conforme a todo lo que en él está escrito; porque entonces harás prosperar tu camino, y todo te saldrá bien._

En Josué 1:8 Dios nos habla de tres partes:
1. Nunca se apartará de tu boca este libro de la ley, sino que de día y de noche meditarás en él
2. Para que guardes y hagas conforme a todo lo que en él está escrito;
3. Porque entonces harás prosperar tu camino, y todo te saldrá bien.

El punto uno habla de dos cosas «de no apartar el libro de la ley de nuestra boca y meditar» lo cual nos ayudará al punto dos «a guardar y hacer la palabra de Dios» y como consecuencia de esto al punto tres «haremos prosperar nuestro camino y todo nos saldrá bien».

Pero para este caso veremos la parte de «Nunca se apartará de tu boca este libro de la ley». 
Y esta parte de «Nunca» hace referencia a una eternidad, ya que la palabra de Dios es eterna, porque permanece para siempre, es decir, es un mandato eterno. También se puede traducir esta parte de «Nunca se apartará» como «No dejes de tener en tu boca este libro de la ley» o «No te retractes de tener en tu boca este libro de la ley», es decir, no tengas una apostasía en tus labios, debemos perseverar en tener en nuestra boca la palabra de Dios.

Ahora tenemos varios puntos que ver sobre esto porque la primer pregunta sería ¿Debo hablar siempre la ley de Dios? La respuesta concreta es «Sí». ¿Tenemos que ser Biblias andando? La respuesta concreta es «Sí». ¿Acaso esto es anticuado y religioso? La respuesta concreta es «No».

Comprendamos sobre la esencia de esto, la ley de Dios representa muchas cosas aunque todo encerrado en esto «Vida Eterna».

La ley de Dios es sabiduría, inteligencia, amor, diligencia, justicia, fe, misericordia, vida, verdad, luz, paz, sanidad, salvación, y mucho más.

_*Mateo 23:23* ¡Ay de vosotros, escribas y fariseos, hipócritas! porque diezmáis la menta y el eneldo y el comino, y dejáis lo más importante de la ley: la justicia, la misericordia y la fe. Esto era necesario hacer, sin dejar de hacer aquello._

_*Génesis 1:3* Y dijo Dios: Sea la luz; y fue la luz._

_*Lucas 10:5* En cualquier casa donde entréis, primeramente decid: Paz sea a esta casa._

_*Deuteronomio 4:6* Guardadlos, pues, y ponedlos por obra; porque esta es vuestra sabiduría y vuestra inteligencia ante los ojos de los pueblos, los cuales oirán todos estos estatutos, y dirán: Ciertamente pueblo sabio y entendido, nación grande es esta._

_*Hebreos 4:12* Porque la palabra de Dios es viva y eficaz, y más cortante que toda espada de dos filos; y penetra hasta partir el alma y el espíritu, las coyunturas y los tuétanos, y discierne los pensamientos y las intenciones del corazón._

_*Mateo 12:34-36* ¡Generación de víboras! ¿Cómo podéis hablar lo bueno, siendo malos? Porque de la abundancia del corazón habla la boca. El hombre bueno, del buen tesoro del corazón saca buenas cosas; y el hombre malo, del mal tesoro saca malas cosas. Mas yo os digo que de toda palabra ociosa que hablen los hombres, de ella dará cuenta en el día del juicio._

Lo que Dios nos está diciendo es que primeramente debemos leer su palabra para conocer todo lo que su palabra representa y da, y cuando hablemos, hablaremos lo que hemos visto y oído. Es como un bebé que termina hablando lo que escucha porque se dedica a escuchar y luego de todo lo que asimila busca hablarlo, tanto con las palabras como con el sentido de las palabras que oyó.

_*Juan 8:38* Yo hablo lo que he visto cerca del Padre; y vosotros hacés lo que habéis oído cerca de vuestro padre._

_*Juan 12:49-50* Porque yo no he hablado por mi propia cuenta; el Padre que me envió, él me dio mandamiento de lo que he de decir, y de lo que he de hablar. Y sé que su mandamiento es vida eterna. Así, pues, lo que yo hable, lo hablo como el Padre me lo ha dicho._

_*Juan 15:15* Ya no os llamaré siervos, porque el siervo no sabe lo que hace su señor; pero os he llamado amigos, porque todas las cosas que oí de mi Padre, os las he dado a conocer._

Por esto es importante leer la Biblia en voz alta además de estar atentos a lo que Dios nos quiera revelar para escuchar y así poder hablar lo que hemos visto y oído.

Por otro lado debemos orar ya que en la oración Dios hablará de igual forma a nuestra vida y eso que hemos oído o visto en visión lo terminaremos hablando.

_*1 Juan 1:3* lo que hemos visto y oído, eso os anunciamos, para que también vosotros tengáis comunión con nosotros; y nuestra comunión verdaderamente es con el Padre, y con su Hijo Jesucristo._

La primer parte nos lleva al punto dos de poner por obra la ley de Dios pero debemos tomar en cuenta que la Biblia habla de dejar de hacer lo malo y una de las cosas que este mundo nos ha enseñado es a hablar injusticias, hablar de forma perezosa, hablar de vanidad y burlarse de las cosas y personas, y todo esto es algo que Dios quiere que cambiemos por eso mismo nos habla de dejar de hacer lo malo y aprender a hacer lo bueno, y esto conlleva hablar bien, es decir, nunca apartar nuestra boca del libro de la ley, esto es, hablar justicia, hablar con rectitud, hablar en paz, hablar con fe, hablar con amor.

_*Isaías 1:16-17* Lavaos y limpiaos; quitad la iniquidad de vuestras obras de delante de mis ojos; dejad de hacer lo malo; aprended a hacer el bien; buscad el juicio, restituid al agraviado, haced justicia al huérfano, amparad a la viuda._

_*1 Pedro 1:18* Sabiendo que fuisteis rescatados de vuestra vana manera de vivir, la cual recibisteis de vuestros padres, no con cosas corruptibles, como oro o plata,_

Pero para dejar de hacer lo malo, primero debemos saber que lo que estamos haciendo es malo y esto únicamente nos lo enseñará «El Espíritu de Verdad», El Espíritu Santo, ya sea que nos hable o nos lleve a su palabra, lo importante es que él nos guiará a toda verdad porque él nos recordará todo lo que Jesucristo ya enseñó. Además que una de las fuentes que El Espíritu de Verdad tomará es la ley, es decir, la Biblia, porque él fue quien inspiro la palabra y usará este medio para guiarnos.

_*Juan 16:7-8* Pero yo os digo la verdad: Os conviene que yo me vaya; porque si no me fuera, el Consolador no vendría a vosotros; mas si me fuere, os lo enviaré. Y cuando él venga, convencerá al mundo de pecado, de justicia y de juicio._

_*Juan 16:13* Pero cuando venga el Espíritu de verdad, él os guiará a toda la verdad; porque no hablará por su propia cuenta, sino que hablará todo lo que oyere, y os hará saber las cosas que habrán de venir._

_*2 Timoteo 3:16* Toda la Escritura es inspirada por Dios, y útil para enseñar, para redargüir, para corregir, para instruir en justicia,_

Si queremos tener el final de «prosperar en lo que hacemos y que todo nos salga bien» debemos iniciar con «nunca apartar de nuestra boca el libro de la ley».

Espero que inicie en la oración y el ministerio de la palabra para que su boca sea para glorificar el nombre de Cristo Jesús y así poder bendecirle en todo tiempo con su manera de hablar. Y que nunca se aparte de su boca el libro de la ley. Amén.